import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeserializationTest {

    private ObjectMapper om = new ObjectMapper();

    @Test
    public void testSerialize() throws JsonProcessingException {

        Ebook ebook = new Ebook();
        ebook.setId("BOL_123");
        ebook.setNumberOfPages(512);
        ebook.setLoanFormat("ebook");

        Eaudiobook eaudiobook = new Eaudiobook();
        eaudiobook.setId("BOL_9901");
        eaudiobook.setDuration(65454);
        eaudiobook.setLoanFormat("eaudiobook");

        List<Product> products = List.of(ebook, eaudiobook);

        String json = om.writeValueAsString(products);

        assertEquals(json, "[{\"id\":\"BOL_123\",\"loanFormat\":\"ebook\",\"numberOfPages\":512},{\"id\":\"BOL_9901\",\"loanFormat\":\"eaudiobook\",\"duration\":65454}]");
    }

    @Test
    public void testDeserialize() throws JsonProcessingException {
        Product p = om.readValue("{\"id\":\"BOL_123\",\"loanFormat\":\"ebook\",\"numberOfPages\":512}", Product.class);
        System.out.println(p);
    }


}

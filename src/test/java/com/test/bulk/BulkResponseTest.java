package com.test.bulk;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.List;


class BulkResponseTest {

    private static ObjectMapper om = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

    @Test
    public void test() {
        ProgressDTO dto1 = new ProgressDTO();
        dto1.setProductId("BOL_111");

        ProgressDTO dto2 = new ProgressDTO();
        dto2.setProductId("BOL_112");

        ProgressDTO dto3 = new ProgressDTO();
        dto3.setProductId("BOL_113");

        var dtos = List.of(dto1, dto2, dto3);

        var response = BulkResponse.fromList(dtos);
        response.addItem(BulkResponseItem.ok(dto3));
        response.addItem(BulkResponseItem.ok(dto1));
        response.addItem(BulkResponseItem.ok(dto2));

        System.out.printf(toJson(response));
    }

    @Test
    public void test2() {
        ProgressDTO dto1 = new ProgressDTO();
        dto1.setProductId("BOL_111");

        ProgressDTO dto2 = new ProgressDTO();
        dto2.setProductId("BOL_112");

        ProgressDTO dto3 = new ProgressDTO();
        dto3.setProductId("BOL_113");

        var dtos = List.of(dto3, dto1, dto2);

        var response = BulkResponse.fromList(dtos);
        response.addItem(BulkResponseItem.ok(dto2));
        response.addItem(BulkResponseItem.invalid(dto3));
        response.addItem(BulkResponseItem.ok(dto1));

        System.out.printf(toJson(response));
    }

    private static String toJson(BulkResponse response) {
        try {
            return om.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnumTest {
    private ObjectMapper om = new ObjectMapper()
            .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
            .enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);

    @Test
    public void testUpper() throws JsonProcessingException {
        assertEquals(AgeCategory.ADULT, parse("\"ADULT\""));
    }

    @Test
    public void testLower()  {
        assertEquals(AgeCategory.ADULT, parse("\"adult\""));
    }

    @Test
    public void testMixed() {
        assertEquals(AgeCategory.ADULT, parse("\"aDuLt\""));
    }

    @Test
    public void testUnknown() {
        assertEquals(AgeCategory.UNKNOWN, parse("\"senior\""));
    }

    @Test
    public void testUpper2() {
        assertEquals(AgeCategory.YOUNG_ADULT, parse("\"YOUNG_ADULT\""));
    }

    @Test
    public void testAlias() {
        assertEquals(AgeCategory.YOUNG_ADULT, parse("\"Young Adult\""));
    }

    @Test
    public void testAlias2() {
        assertEquals(AgeCategory.YOUNG_ADULT, parse("\"Teenager\""));
    }

    @Test
    public void testSerializeYoungAdult() {
        assertEquals("\"Young Adult\"", serialize(AgeCategory.YOUNG_ADULT));
    }

    private AgeCategory parse(String s) {
        try {
            return om.readValue(s, AgeCategory.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String serialize(AgeCategory a) {
        try {
            return om.writeValueAsString(a);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}

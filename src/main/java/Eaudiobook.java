public class Eaudiobook extends Product {
    private int duration;

    public Eaudiobook() {
        super("eaudiobook");
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Eaudiobook{" +
                "duration=" + duration +
                "} " + super.toString();
    }
}

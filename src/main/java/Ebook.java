public class Ebook extends Product{
    private int numberOfPages;

    public Ebook() {
        super("ebook");
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    @Override
    public String toString() {
        return "Ebook{" +
                "numberOfPages=" + numberOfPages +
                "} " + super.toString();
    }
}

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonProperty;

public enum AgeCategory {
    @JsonProperty("Adult")
    ADULT,
    @JsonAlias({"Young Adult", "YOUNG_ADULT", "Teenager"})
    @JsonProperty("Young Adult")
    YOUNG_ADULT,
    @JsonProperty("Children")
    CHILDREN,
    @JsonEnumDefaultValue
    @JsonProperty("Unknown")
    UNKNOWN;

}

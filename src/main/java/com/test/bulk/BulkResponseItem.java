package com.test.bulk;

public class BulkResponseItem <T extends Identifiable>{
    private String id;
    private int code;
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static <T extends Identifiable> BulkResponseItem<T> ok(T data) {
        BulkResponseItem<T> item = new BulkResponseItem<>();
        item.setId(data.getId());
        item.setCode(200);

        return item;
    }

    public static <T extends Identifiable> BulkResponseItem<T> invalid(T data) {
        BulkResponseItem<T> item = new BulkResponseItem<>();
        item.setId(data.getId());
        item.setCode(400);

        return item;
    }
}

package com.test.bulk;

public interface Identifiable {
    String getId();
}

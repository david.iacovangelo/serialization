package com.test.bulk;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.*;
import java.util.stream.Collectors;

public class BulkResponse<T extends Identifiable> {
    private List<String> ids;
    private Map<String, BulkResponseItem<T>> idToItem = new HashMap<>();
    private boolean errors;

    @JsonGetter("items")
    public Collection<BulkResponseItem<T>> items() {
        Collection items = new ArrayList<>();
        for(String id : ids) {
            items.add(idToItem.get(id));
        }
        return items;
    }

    public boolean isErrors() {
        return errors;
    }

    private BulkResponse(List<String> ids) {
        this.ids = ids;
    }

    public void addItem(BulkResponseItem<T> item) {
        String id = item.getId();
        idToItem.put(id, item);
        if(item.getCode() >= 400) {
            errors = true;
        }
    }

    public static <T extends Identifiable> BulkResponse fromList(List<T> items) {
        List<String> ids = items.stream()
                .map(Identifiable::getId)
                .collect(Collectors.toList());
        return new BulkResponse<T>(ids);
    }
}

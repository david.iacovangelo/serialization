import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "loanFormat",
        defaultImpl = UnknownProduct.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Ebook.class, name = "ebook"),
        @JsonSubTypes.Type(value = Eaudiobook.class, name = "eaudiobook")
})
abstract class Product {

    private String id;
    private String loanFormat;

    protected Product(String loanFormat) {
        this.loanFormat = loanFormat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoanFormat() {
        return loanFormat;
    }

    public void setLoanFormat(String loanFormat) {
        this.loanFormat = loanFormat;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", loanFormat='" + loanFormat + '\'' +
                '}';
    }
}
